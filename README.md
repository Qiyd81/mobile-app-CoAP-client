Features:
Wifi communication to ESP8266 using CoAP protocol.
Android App acts as a CoAP Client sending GET and POST messages to the ESP8266 server.


Android APP progress:

Progress vid 1: https://www.youtube.com/watch?v=g_WMk7fxhXY

Progress vid 2: https://www.youtube.com/watch?v=wF9sT_k4e_Y&feature=youtu.be

Usage:
Enable Wifi in the phone drop down menu.<br /> 
*  This allows CoAP client to connect to the server socket
    

Set Destination in the map fragment by tapping on the map


![MainMap](/uploads/7a2fc855aa2ab7f6bcb65c5a3bbdc7df/MainMap.jpg)



Press "Send destination coordinates button"<br />
*  This will send a CoAP PUT message with signed latitude and longitude coordinates.


Press "Start"<br />
*  Sends  CoAP PUT message to the "/setStatus" resource
    

Click on the 'Sensor Info' Tab to see sensor values



![SensorScreen](/uploads/218ed84f4c908d9037a9cd0ad3a99a5a/SensorScreen.jpg)